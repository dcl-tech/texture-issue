/// --- Spawner function ---

function spawnCube(x: number, y: number, z: number) {
  // create the entity
  const cube = new Entity()

  // add a transform to the entity
  cube.addComponent(new Transform({ position: new Vector3(x, y, z) }))

  // add a shape to the entity
  cube.addComponent(new BoxShape())

  // add the entity to the engine
  engine.addEntity(cube)

  return cube
}

/// --- Spawn a cube ---

// //const t1 = new Texture("materials/YourAdHere_0.8.png")
// const t1 = new Texture("materials/image1.png")
// const m1 = new Material()
// m1.albedoTexture = t1
// const cube1 = spawnCube(4, 1, 8)
// cube1.addComponent(m1)

////// These image names produce red questionmarks
//const t2 = new Texture("materials/YourAdHere_0.8.jpg") // QuestionMark
//const t2 = new Texture("materials/YourAdHerx_0.8.jpg") // QuestionMark
//const t2 = new Texture("materials/YourAdHere.jpg") // QuestionMark
const t2 = new Texture("materials/Advertise.jpg") // QuestionMark

////// These image names work fine.
//const t2 = new Texture("materials/Advertis.jpg") // ok
//const t2 = new Texture("materials/YrAd_0.8.jpg") // ok
// const t2 = new Texture("materials/image08.jpg") // ok
//const t2 = new Texture("materials/_YourAdHere.jpg") // ok
//const t2 = new Texture("materials/YourAdHereX.jpg") // ok
//const t2 = new Texture("materials/YourHere.jpg") // ok
//const t2 = new Texture("materials/AdHere.jpg") // ok
//const t2 = new Texture("materials/image1.jpg") // ok
const m2 = new Material()
m2.albedoTexture = t2
const cube2 = spawnCube(8, 1, 8)
cube2.addComponent(m2)
